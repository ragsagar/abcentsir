
##Target Audience Research

How Many?
**************
Of the 100 million internet users of India 11.7 % are students. 

Who and What they are doing?
*************************************
The demographics of the students are in the following way

--Schooling Until Age 16 - 0.9%
--Schooling Until Age 18 - 3.1%
--Trade/Technical School Or College - 14.7%
--University - 44.9%
--Post Graduate -- 36.5%

*source: http://www.imarks.in/resources/DigitalMarketingStatistics.pdf

As target audience is from different cultural backgrounds and different language speaking people, its logical to stick on to use only English for all communications including captions and other puns statements.

Web Savvy
*************
From current popularity of social networking websites among students its assumable that majority of internet users are intermediate in using web based interfaces. So its logical to go for a full fledged functional yet a simple interface based on popular conventions.

What device?
****************
Apart from desktop computers a vast majority of Indian students population is using mobile phones ( & tablets ) to browse the web. So it is logical to go for a responsive web design to address mobile web users as well.

Other findings based on target audience behavior
**************************************************************
Clear statements and factual information about the products we are offering will be very convincing for students as it will help them to choose wise decisions on purchases. So it is important to highlight offers characteristics like amount they will be saving, actual price discounted price etc.


Please leave comments about other characteristics or behavior of the target audience which you find is important to note in the design process.


##Recommended read list

http://iampaddy.com/lifebelow600/

http://uxdesign.smashingmagazine.com/2012/07/11/better-product-pages-turn-visitors-into-customers/

http://www.smashingmagazine.com/2009/11/10/designing-coming-soon-pages/

http://www.smashingmagazine.com/2011/09/01/elements-of-a-viral-launch-page/

http://sixrevisions.com/content-strategy/about-page-guidelines/

http://www.smashingmagazine.com/2008/01/15/isnt-it-sweet-mascots-in-modern-web-design/

http://vector.tutsplus.com/tutorials/illustration/how-to-create-a-web-site-mascot/

http://tympanus.net/codrops/2011/07/07/25-examples-of-inspiring-product-display/

##Functional Specifications

1) Student Registration, Authentication and Profile Management
• Facebook and linkedIn login and regular registration
• Request a invite feature for the students.
• Whoever receives an invite can only register
• At the time of going live, people who have registered will be sent invites to register (this is
one time activity only)
• Automatic invitation after defined days of Invite request with account activation url
• Enabling scanned id card (2 id cards will be uploaded - college id card and other id
verification) uploading and follow up manual checking if no email id is available
• Student profile page and editing capabilities.
2) Super Admin Module
• Uploading offers along with image, brief description and detailed description. Also, pointer
to refer to merchant site or lead capture form. lead capture details to be emailed to
prespecified email addresses
• Superadmin will be able to manage the site level activites
• Superadmin will be able to add and delete various objects - offers, images
• To moderate students and vendors - students only and not vendors. Students to be
approved as per the process in emailed specs
• To moderate the commenting system - postponed for now
3) Offer Display
• Displaying offers based on various filters
• Students will be able to "I am interested" on offers
• One more option to “Contact us for group offers”. email will be sent to super admin
• Students will be able to comment and discuss on offers
• Students can like and promote offers
• Students will be able to share it on social media
4) Request Engine
• Features to help students request specific offers - email to be sent to super admin
• Features to help students request for group offers
5) Commenting System
• Facebook integration
• Updates and follow up notifications
o
e confirm wa



