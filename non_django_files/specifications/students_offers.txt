Functional Specifications

Concept : Online Community for Students

Here, brands across laptops, mobiles, fast food, clothing, stationary
and anything that is relevant during student life can make
student-specific offers. Students on the other hand can take advantage
of this and also request specific offers from Brands.


Desired Functionality

For Brands
1) Shopping Cart - Brands can create student specific offers and we
can place a cart for the transaction. The transaction in turn would be
physically fulfilled by the Brand. e.g. DELL can place a cart with
special offers for Student Laptops.

2) Demand Generation - Here, there will not be any cart. There will
just a FORM which a student would fill saying that he is interested in
a particular product or service. This will be like a lead generation
mechanism.

3) Promotion Window - Here, brands can showcase their specific offers.
It is not an advertisement, just a promotion.

For Students
4) Request Engine - When there are no offers or a student is keen on a
particular product, we would like to give them an opportunity to
request an offer. Then all the offers can be combined into a basket
and that particular brand can propose an offer to all the students who
had requested it.

************************************

