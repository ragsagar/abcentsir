$(document).ready(function() {

// Support for AJAX loaded modal window.
// Focuses on first input textbox after it loads the window.
$('[data-toggle="modal"]').bind('click',function(e) {
	e.preventDefault();
	var url = $(this).attr('href');
	if (url.indexOf('#') == 0) {
		$('#response_modal').modal('open');
	} else {
		$.get(url, function(data) {
                        $('#response_modal').html(data);
                        $('#response_modal').modal();
		}).success(function() { $('input:text:visible:first').focus(); });
	}
});

});