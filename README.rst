==========
AbCent Sir
==========

A Collective Buying Service for Students.
=========================================

Setting up postgres database for development
--------------------------------------------

Lets create an user `test_user` with password `test_password` and grant all
permissions to the database `test_database`.

- In the shell as root,

# su - postgres

$ psql -u template1

# CREATE USER test_user WITH PASSWORD 'test_password';

# CREATE DATABASE test_db;

# GRANT ALL PRIVILEGES ON DATABASE test_db to test_user;

# \q

- Then as root edit /var/lib/pgsql/data/pg_hba.conf
- Then change the lines to look like this
local   all     all                 trust
host    all     all     ::1/128     trust

- Then run the following command in the project root directory to create the
database tables needed for the project. 

$ python manage.py syncdb

- To run the development server issue the following command.

$ python manage.py runserver

Things to follow.
-----------------
- All static files should be in AbCentSir/static/ directory.
- All html templates should be in AbCentSir/templates/ directory.
- All user uploaded files will be in AbCentSir/media/ directory.

Use the following command to update the database ( depends on django-south )
$ ./manage.py migrate south

**IMPORTANT**
Instead of pip installing django-mailer, git clone and setup.py install django
mailer. Otherwise send_mail command won't work.
