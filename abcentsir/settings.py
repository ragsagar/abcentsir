# Django settings for abcentsir project.
import os
from django.core.urlresolvers import reverse_lazy

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# To avoid hardcoding of path
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

ADMINS = (
     ('ragsagar', 'ragsagar@gmail.com'),
     ('adhil', 'adhilazeeznv@gmail.com')
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'testdb',                      # Or path to database file if using sqlite3.
        'USER': 'test_user',                      # Not used with sqlite3.
        'PASSWORD': 'test_password',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Kolkata'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
#STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = ( os.path.join(PROJECT_ROOT, 'static'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '714zy66h4(7mj=29m**(2zxlrdyg%02+%o7hm+y+ekt-xb5mgo'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'abcentsir.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'abcentsir.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_ROOT, 'templates')
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    # third party apps
    # abcentsir apps
    'abcentsir.apps.auth',
    'abcentsir.apps.offers',
    'abcentsir.apps.abadmin',
    'mailer',
    'crispy_forms',
    'social_auth',
    'endless_pagination',
    #'gunicorn',
    #'south',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': 'logs/mylog.log',
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 5,
            'formatter':'standard',
        },  
        'request_handler': {
                'level':'DEBUG',
                'class':'logging.handlers.RotatingFileHandler',
                'filename': 'logs/django_request.log',
                'maxBytes': 1024*1024*5, # 5 MB
                'backupCount': 5,
                'formatter':'standard',
        },
    },
    'loggers': {

        '': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
        },
        'django.request': { # Stop SQL debug from logging to main logger
            'handlers': ['request_handler'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}

# Email Settings
DEFAULT_FROM_EMAIL = 'ragsagar.digizone@gmail.com'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'ragsagar.digizone@gmail.com'
EMAIL_HOST_PASSWORD = 'itsmeniya'
EMAIL_USE_TLS = True


# Settings needed for facebook integration
AUTHENTICATION_BACKENDS = (
        'social_auth.backends.twitter.TwitterBackend',
        'social_auth.backends.facebook.FacebookBackend',
        'social_auth.backends.contrib.linkedin.LinkedinBackend',
        'django.contrib.auth.backends.ModelBackend',
        )

# Credentials for test apps

FACEBOOK_APP_ID = '343469885728994'
FACEBOOK_API_SECRET = 'a24a7a96807c2effc9a48277c1143509'

#LINKEDIN_CONSUMER_KEY = 'j1dbuo1l2eqy'
#LINKEDIN_CONSUMER_SECRET = 'QEjGFPxH9cQUQ1AS'

LINKEDIN_CONSUMER_KEY = 'wxe3hvsong85' 
LINKEDIN_CONSUMER_SECRET = 'nVRjTifyyG4nGImO'

# social auth authentication and completion url names
#SOCIAL_AUTH_COMPLETE_URL_NAME = 'complete'
#SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'associate_complete'

SOCIAL_AUTH_DEFAULT_USERNAME = 'new_social_auth_user'

# To redirect to the current url when a user registers with fb
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = reverse_lazy('auth_verify_as_student')

FACEBOOK_EXTENDED_PERMISSIONS = ['email']
LINKEDIN_EXTRA_FIELD_SELECTORS = ['email-address']
LINKEDIN_REQUEST_TOKEN_EXTRA_ARGUMENTS = {'scope': 'r_emailaddress'}

SOCIAL_AUTH_PIPELINE = (
        'social_auth.backends.pipeline.social.social_auth_user',
        'social_auth.backends.pipeline.associate.associate_by_email',
        'social_auth.backends.pipeline.misc.save_status_to_session',
        'abcentsir.apps.auth.pipeline.get_username',
        'abcentsir.apps.auth.pipeline.create_user',
        #'social_auth.backends.pipeline.user.get_username',
        #'social_auth.backends.pipeline.user.create_user',
        'social_auth.backends.pipeline.social.associate_user',
        'social_auth.backends.pipeline.social.load_extra_data',
        'social_auth.backends.pipeline.user.update_user_details',
        )

AUTH_PROFILE_MODULE = 'auth.StudentProfile'

LOGIN_URL = reverse_lazy('auth_login_user')

# Redirects to this url after login, we can use it to check for student
# verification related thing right after login
LOGIN_REDIRECT_URL = reverse_lazy('home')

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'social_auth.context_processors.social_auth_backends',
    'social_auth.context_processors.social_auth_login_redirect',
    'abcentsir.apps.auth.context_processors.username_generator',
)


# Database settings needed for heroku
import dj_database_url
DATABASES = {'default': dj_database_url.config(default='postgres://test_user:test_password@localhost:5432/testdb')}

##### AbCentSir Specific Settings #####

# Set the following to True to expire the invitation send to students
STUDENT_INVITATION_EXPIRE = False

# Days after which the invitation should expire. Valid only if
# STUDENT_INVITATION_EXPIRE is True
STUDENT_INVITATION_EXPIRY_DAYS = 10

# Mention the number of days after which the email invitations should be sent
SEND_INVITATION_AFTER_DAYS = 2

# if the verification email should expire
EMAIL_VERIFICATION_EXPIRE = True

# Number of days in which the verification url sent should expire
EMAIL_VERIFICATION_EXPIRY_DAYS = 10

