from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse_lazy
from mailer import send_html_mail as send_mail_to_queue

DOMAIN = 'thawing-lake-6984.herokuapp.com'

def send_invitation(activation_key, to_email):
    """
    Sends the activation url to the given email id at the time itself.
    """
    txt_dict = {'url': reverse_lazy('auth_activate',
                       kwargs={'activation_key': activation_key}),
                'domain': DOMAIN}
    subject = render_to_string('auth/invitation_email_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/invitation_email.txt', txt_dict)
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()


def send_invitation_later(activation_key, to_email):
    """
    Pushes the email inivtation to the mail queue. Send all the mails in the
    queue when the send_mail admin command is called. This is used for sending
    emails at later instance.
    """
    txt_dict = {'url': reverse_lazy('auth_activate',
                       kwargs={'activation_key': activation_key}),
                'domain': DOMAIN}
    subject = render_to_string('auth/invitation_email_subject.txt')
    # line breaks are not allowed in subject
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/invitation_email.txt', txt_dict)
    send_mail_to_queue(subject, message, message, settings.DEFAULT_FROM_EMAIL,
                       [to_email])
    

def send_verification_mail(key, to_email):
    """
    Sends the verification email with the given key to the given email address.
    Verification mail is send to the email id user gives during registration.
    """
    key_dict = {'url': reverse_lazy('auth_verify_alternate_email',
                                    kwargs={'verification_key': key}),
                'domain': DOMAIN}
    subject = render_to_string('auth/verification_mail_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/verification_mail.txt', key_dict)
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()


def send_student_verification_mail(key, to_email):
    """
    Same as that of send_verification_mail method. Only the subject and mail
    content differs.
    """
    key_dict = {'url': reverse_lazy('auth_verify_student_email', 
                                    kwargs={'verification_key': key}),
                'domain': DOMAIN}
    subject = render_to_string('auth/student_verification_mail_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/student_verification_mail.txt', key_dict)
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()


def send_student_verification_success_mail(user):
    """
    Sends a success email when the user is successfully verified as a student.
    Accepts user object and determines to_email from it.
    """
    user_profile = user.get_profile()
    #if user_profile.registered_email:
        #to_email = user_profile.registered_email
    #else:
        #to_email = user.username
    to_email = user_profile.get_primary_email()
    subject = render_to_string('auth/student_verification_success_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/student_verification_success_mail.txt')
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()


def send_forgot_password_mail(key, to_email):
    """
    Sends the forgot password mail to the user with the link to reset
    password.
    """
    key_dict = {'url': reverse_lazy('auth_onetime_change_password',
                                    kwargs={'key': key}),
                'domain': DOMAIN}
    subject = render_to_string('auth/forgot_password_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/forgot_password_mail.txt', key_dict)
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()


def send_idcard_verification_rejection_notification(user, comment=None):
    """
    Sends the notification that the request for id card based student
    verification was rejected.
    """
    user_profile = user.get_profile()
    to_email = user_profile.get_primary_email()
    data_dict = {'comment': comment}
    subject = render_to_string('auth/id_verification_rejection_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/id_verification_rejection_mail.txt',
                               data_dict)
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()

    
def send_email_verification_rejection_notification(user, student_email=None):
    """
    Sends the notification that the request for email based student
    verification was rejected.
    """
    user_profile = user.get_profile()
    to_email = user_profile.get_primary_email()
    data_dict = {'student_email': student_email}
    subject = render_to_string('auth/mail_based_verification_rejection_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/mail_based_verification_rejection_mail.txt',
                               data_dict)
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()


def send_wait_for_invitation_mail(to_email):
    """
    If a user is not getting invitation right after requesting for invite this
    mail will be send to them.
    """
    subject = render_to_string('auth/invitation_waiting_subject.txt')
    subject = ' '.join(subject.splitlines())
    message = render_to_string('auth/invitation_waiting_mail.txt')
    msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [to_email])
    msg.content_subtype = 'html'
    msg.send()

