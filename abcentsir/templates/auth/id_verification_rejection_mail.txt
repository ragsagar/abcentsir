Hey. Sorry we are unable to confirm your Student Credentials because:<br>
<br>
{{ comment }} <br>
<br>
We request you to follow these steps. <br>
<br>
Step 1 : Login again at www.abcentsir.com with your login id and password <br>
Step 2 : Click on “I am a Student, Verify Now” at the top Right Hand Corner of the screen <br>
Step 3 : Complete the verification process by putting in the correct student email address or alternately by attaching the relevant student id. <br>
<br>
Come on in, the Class is waiting.<br>
<br>
All the best!<br>
<br>
- AbCentiyas at the AbCent Sir Team