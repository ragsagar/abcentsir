import datetime
import re
import hashlib
import random

from urllib2 import urlopen, HTTPError
from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.conf import settings
from django.template.defaultfilters import slugify
from django.core.files.base import ContentFile

from social_auth.signals import pre_update
from social_auth.backends.facebook import FacebookBackend
from social_auth.backends.contrib.linkedin import LinkedinBackend

from abcentsir.libs import (send_invitation, send_invitation_later,
        send_verification_mail, send_student_verification_mail,
        send_student_verification_success_mail, send_forgot_password_mail,
        send_wait_for_invitation_mail)

try:
    from django.utils.timezone import now as datetime_now
except ImportError:
    datetime_now = datetime.datetime.now


SHA1_RE = re.compile('^[a-f0-9]{40}$')


class StudentProfile(models.Model):
    """
     Makes use of UserProfile to extend the django User model to have
     additional fields required for Students. Can be accessed like
     Userobject.get_profile().field
    """
    user = models.OneToOneField(User)
    mobile_number = models.BigIntegerField(max_length=15, null=True, blank=True)
    address = models.CharField(max_length=100, blank=True)
    college = models.CharField(max_length=300, blank=True)
    month_of_passing = models.CharField(max_length=15, blank=True)
    year_of_passing = models.IntegerField(max_length=4, null=True, blank=True)
    # Username, which can be set only once.
    user_name = models.CharField(max_length=30, blank=True)
    # If user is registering for invite with student email, then User.email
    # should be copied here.
    student_email = models.EmailField(max_length=75, blank=True)
    # If the user enters another email during registration for communication
    # the email used for invitation will be copied to the following field and
    # that email will be stored in user.email
    invited_email = models.EmailField(max_length=75, blank=True)
    # The email user adds in user profile
    registered_email = models.EmailField(max_length=75, blank=True)
    #user registered, to determine if we should send mails to this mail id
    use_registered_email = models.BooleanField(default=False)
    facebook_share = models.BooleanField(default=True)
    # To store the profile image.
    profile_image = models.ImageField(
                        upload_to='profile_images/%Y/%m/%d',
                        blank=True)
    other_id_document = models.ImageField(
                                    upload_to='student_profile/other_ids',
                                    blank=True)
    college_id_card = models.ImageField(
                                upload_to='student_profile/college_ids',
                                blank=True)
    verified_as_student = models.BooleanField(default=False)
    # True for a user who presses not a student during student verification
    not_student = models.BooleanField(default=False)
    # To denote the state of waiting for admin approval after applying for
    # verification.
    applied_for_verification = models.BooleanField(default=False)
    # To check if the user has connected his account with facebook

    def get_primary_email(self):
        """
        Returns the email used for requesting for invite if
        user_registered_email is False, otherwise returns user_registered_email
        """
        if self.registered_email and self.use_registered_email:
            return self.registered_email
        else:
            return self.user.username

    def confirm_as_student(self):
        """
        Called when a user is successfully verified as a students.
        Sets and resets needed flags to reflect as a verified student.
        """
        self.applied_for_verification = False
        self.not_student = False
        self.verified_as_student = True
        self.save()

    def wait_for_confirmation(self):
        """
        Sets applied_for_verification and resets every other flag.
        """
        self.applied_for_verification = True
        self.not_student = False
        self.verified_as_student = False
        self.save()

    def verify_later(self):
        """
        Sets flags which reflects a student who opts to verify later.
        """
        self.applied_for_verification = False
        self.not_student = False
        self.verified_as_student = False
        self.save()

    def confirm_as_not_student(self):
        """
        Sets flags such that it reflects like the user is not a student.
        """
        self.applied_for_verification = False
        self.verified_as_student = False
        self.not_student = True
        self.save()


def create_student_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = StudentProfile.objects.get_or_create(user=instance)

post_save.connect(create_student_profile, sender=User)


def facebook_linkedin_extra_values(sender, user, response, details, **kwargs):
    user_profile = user.get_profile()
    dump, created = SocialAuthDump.objects.get_or_create(user=user,
                                              backend=sender.name)
    if created:
        dump.data = str(response)
        dump.save()
    else:
        if dump.data != str(response):
            dump.data = str(response)
            dump.save()
    if not user_profile.registered_email:
        if isinstance(sender, LinkedinBackend):
            email = response.get('email-address') or ''
        else:
            email = response.get('email') or ''
        if email and (email != user.username):
            user_profile.registered_email = email
    if not user.first_name:
        user.first_name = response.get('first_name') or ''
    if not user.last_name:
        user.last_name = response.get('last_name') or ''
    if 'id' in response:
        if not user_profile.profile_image:
            try:
                url = None
                if sender == FacebookBackend:
                    url = "http://graph.facebook.com/%s/picture?type=large" \
                                % response["id"]
                #elif sender == google.GoogleOAuth2Backend and "picture" in response:
                    #url = response["picture"]
    
                if url:
                    avatar = urlopen(url)
                    
                    user_profile.profile_image.save(slugify(
                                        user.username + " social") + '.jpg',
                                        ContentFile(avatar.read()))              
            except HTTPError:
                pass
    user_profile.save()
    user.save()
    

pre_update.connect(facebook_linkedin_extra_values, sender=FacebookBackend)
pre_update.connect(facebook_linkedin_extra_values, sender=LinkedinBackend)

class EmailRegistrationManager(models.Manager):
    """
    Custom manager for the StudentEmailRegistrationProfile.
    Different methods defined here helps in activation_key generation, Student
    creation and expired EmailRegistrationProfile deletion.
    """
    def get_profile_from_key(self, activation_key):
        """
        Returns the corresponding profile instance if a valid activation_key is
        given. Otherwise returns False.
        """
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return False
            if not profile.activation_key_expired():
                return profile
        return False

    def create_profile(self, email):
        """
        Created a Registration profile, generates activation_key, stores the 
        activation_key and email in the profile instance and returns it.
        """
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        if isinstance(email, unicode):
            email = email.encode('utf-8')
        activation_key = hashlib.sha1(salt+email).hexdigest()
        profile, created = self.get_or_create(email=email)
        if not created and not profile.activation_key_expired():
            return profile
        profile.activation_key = activation_key
        profile.save()
        return profile

    def delete_expired(self):
        """
        This method will be used to delete all the profile instance that are
        expired and already used.
        """
        if not settings.STUDENT_INVITATION_EXPIRE:
            return

        for profile in self.all():
            if profile.activation_key_expired():
                profile.delete()


class EmailRegistrationProfile(models.Model):
    """
    Instance of this model will be created when a student registers for an
    invite. After SEND_INVITATION_AFTER_DAYS
    days of registration, a  mail will be send to the student's
    email id. When the student clicks the activation link sent, a user instance
    will be created and this self.email will be copied into it. Optionally this
    profile instance can be deleted as it doesn't serve any purpose after that.
    (Optional) if the user is not clicking the activation link within specified
    days the profile will expire.
    """
    # The activation key will be mailed to the email id
    email = models.EmailField()

    # Activation key will be generated when the user registers for invitation.
    # This activation key will be mailed to them after few days
    activation_key = models.CharField(max_length=40)
    
    # Time at which the user registered for invitation
    registered_time = models.DateTimeField(auto_now_add=True)

    objects = EmailRegistrationManager()

    def __unicode__(self):
        return u'Activation Information for email %s' % self.email

    def activation_key_expired(self):
        """
        Checks if settings support activation key expiration. If
        STUDENT_INVITATION_EXPIRE in settings is False, returns False without
        checking anything. If it is True, checks if the activation key has
        expired. Activation key is expired if `registered_time` + 
        STUDENT_INVITATION_EXPIRY_DAYS in settings is less than current time.
        """
        if not settings.EMAIL_VERIFICATION_EXPIRE:
            return False

        expiration_date = \
            datetime.timedelta(days=settings.STUDENT_INVITATION_EXPIRY_DAYS)
        return (self.registered_time + expiration_date) <= datetime_now()
    
    def send_invitation_email(self):
        """
        Sends the activation email to the registered mail ids. Sets to send
        email after settings.SEND_INVITATION_AFTER_DAYS. Uses celery to execute
        the send_mail task after the mentioned days.
        """
        send_invitation(self.activation_key, self.email)

    def send_invitation_email_later(self):
        """
        Adds the mail to the django mailer queue. A cron will be set to
        periodically send all the mails in the django mailer queue.
        This will be used for sending invitations at a later occassion.
        """
        send_invitation_later(self.activation_key, self.email)
        send_wait_for_invitation_mail(self.email)


class EmailVerificationManager(models.Manager):
    """
    Defines the table level methods needed by EmailVerification profile
    instances.
    """
    def create_profile(self, user, email):
        """
        Created a Registration profile, generates activation_key, stores the 
        activation_key and email in the profile instance and returns it.
        """
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        if isinstance(email, unicode):
            email = email.encode('utf-8')
        verification_key = hashlib.sha1(salt+email).hexdigest()
        profile, created = self.get_or_create(user=user)
        if not created and not profile.verification_key_expired():
            return profile
        profile.verification_key = verification_key
        profile.email = email
        profile.save()
        return profile

    def get_profile_from_key(self, verification_key):
        """
        Returns the corresponding profile instance if a valid activation_key is
        given. Otherwise returns False.
        """
        if SHA1_RE.search(verification_key):
            try:
                profile = self.get(verification_key=verification_key)
            except self.model.DoesNotExist:
                return False
            if not profile.verification_key_expired():
                return profile
        return False

    def delete_expired(self):
        """
        This method will be used to delete all the profile instance that are
        expired and already used.
        """
        if not settings.EMAIL_VERIFICATION_EXPIRE:
            return

        for profile in self.all():
            if profile.verification_key_expired():
                profile.delete()


class EmailVerificationProfile(models.Model):
    """
    Used for the verification of email user enters during registration.
    Keeps a temporary instance with the key and user in the database and
    verifies it when the user clicks the verification email.
    """
    VERIFIED = 'ALREADY_ACTIVATED'
    user = models.ForeignKey(User, unique=True, verbose_name='user')
    email = models.EmailField(verbose_name='Email')
    verification_key = models.CharField('verification_key', max_length=40)
    timestamp = models.DateTimeField(auto_now_add=True)
    objects = EmailVerificationManager()

    def __unicode__(self):
        return u"Verification information for %s" % self.user

    def verification_key_expired(self):
        expiration_date = \
                datetime.timedelta(days=settings.EMAIL_VERIFICATION_EXPIRY_DAYS)
        return self.verification_key == self.VERIFIED or \
                (self.timestamp + expiration_date <= datetime_now())

    def send_verification_email(self):
        """
        Used to send verification email during registration. Calls the
        send_verification_email method in abcentsir.libs to send verification
        email with the current instance's email and verification key.
        """
        send_verification_mail(self.verification_key, self.email)


class StudentEmailVerificationManager(models.Manager):
    """
    Defines the table level methods needed by EmailVerification profile
    instances.
    """
    def create_profile(self, user, email, college, month_of_passing,
                       year_of_passing):
        """
        Created a Registration profile, generates activation_key, stores the 
        activation_key and email in the profile instance and returns it.
        """
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        if isinstance(email, unicode):
            email = email.encode('utf-8')
        verification_key = hashlib.sha1(salt+email).hexdigest()
        profile, created = self.get_or_create(user=user)
        #if not created and not profile.verification_key_expired():
            #return profile
        # If tried to create again, the verification key will be reset 
        profile.verification_key = verification_key
        profile.college = college
        profile.email = email
        profile.year_of_passing = year_of_passing
        profile.month_of_passing = month_of_passing
        profile.save()
        return profile

    def get_profile_from_key(self, verification_key):
        """
        Returns the corresponding profile instance if a valid activation_key is
        given. Otherwise returns False.
        """
        if SHA1_RE.search(verification_key):
            try:
                profile = self.get(verification_key=verification_key)
            except self.model.DoesNotExist:
                return False
            if not profile.verification_key_expired():
                return profile
        return False

    def delete_expired(self):
        """
        This method will be used to delete all the profile instance that are
        expired and already used.
        """
        if not settings.EMAIL_VERIFICATION_EXPIRE:
            return

        for profile in self.all():
            if profile.verification_key_expired():
                profile.delete()


class StudentEmailVerificationProfile(models.Model):
    """
    Extends the EmailVerificationProfile and redefines the
    send_verification_email method which needs different url.
    """
    VERIFIED = 'ALREADY_ACTIVATED'
    user = models.ForeignKey(User, unique=True, verbose_name='user')
    email = models.EmailField(verbose_name='Email')
    college = models.CharField(max_length=300, blank=True)
    month_of_passing = models.CharField(max_length=15, blank=True)
    year_of_passing = models.IntegerField(max_length=4, null=True, blank=True)
    verification_key = models.CharField('verification_key', max_length=40)
    timestamp = models.DateTimeField(auto_now_add=True)
    objects = StudentEmailVerificationManager()

    def __unicode__(self):
        return u"Verification information for %s" % self.user

    def verification_key_expired(self):
        expiration_date = \
                datetime.timedelta(days=settings.EMAIL_VERIFICATION_EXPIRY_DAYS)
        return self.verification_key == self.VERIFIED or \
                (self.timestamp + expiration_date <= datetime_now())

    def send_student_verification_email(self):
        """
        To send verification url to student email ids.
        """
        send_student_verification_mail(self.verification_key, self.email)


class EmailListManager(models.Manager):
    """
    Will provide methods to check if an email's extension already exist in
    the database by taking email as input. Also provides methods to add new
    extensions to the database.
    """
    def _extract_extension_from_email(self, email=None):
        """
        Returns the extension of a email id.
        Email should be given as input.
        """
        if email:
            return email.split('@')[-1]

    def contains_extension_of(self, email=None):
        """
        Checks if the database contains the extension of the given email. If it
        is there in the database, the extension will be returned otherwise
        False.
        """
        if email:
            try:
                obj = self.get(
                        extension=self._extract_extension_from_email(email)
                              )
            except self.model.DoesNotExist:
                return False
            return obj.extension
        return False

    def contains(self, ext=None):
        """
        Checks if the database contains the given extension. If it is there,
        the extension will be returned otherwise False.
        """
        if ext:
            try:
                obj = self.get(extension=ext)
            except self.model.DoesNotExist:
                return False
            return ext
        return False

    def add_extension_of(self, email=None):
        """
        Adds the given email's extension to the database if it is not there.
        """
        if email:
            if not self.contains_extension_of(email):
                return self.create(
                        extension=self._extract_extension_from_email(email)
                                  )

    def add(self, ext=None):
        """
        Add the given extension to the database.
        """
        if ext:
            if not self.contains(ext):
                return self.create(extension=ext)

    def remove_extension_of(self, email=None):
        """
        Removes the given email's extension from the database if it is there.
        """
        if email:
            if self.contains_extension_of(email):
                obj = self.get(
                        extension=self._extract_extension_from_email(email)
                              )
                obj.delete()

    def remove(self, ext=None):
        """
        Removes the given extension from the database.
        """
        if ext:
            if self.contains(ext):
                obj = self.get(extension=ext)
                obj.delete()
        

class GreenEmailList(models.Model):
    """
    This model will contain the list of email extensions that are considered as
    email extensions of educational institutions. When a new email extension
    is registered by the user, it will be added here after admin verification.
    """
    extension = models.CharField(max_length=30)
    objects = EmailListManager()

    def __unicode__(self):
        return u"%s" % self.extension


class RedEmailList(models.Model):
    """
    This model will contain the list of email extensions that are not
    considered as email extension of educational institution. Most of the email
    extensions of free email service providers will be added here.
    """
    extension = models.CharField(max_length=30)
    objects = EmailListManager()

    def __unicode__(self):
        return u"%s" % self.extension


#class YellowEmailList(models.Model):
    #"""
    #This model will contain the list of email extensions that are not in
    #GreenEmailList as well as RedEmailList. This email extensions are waiting
    #for manual check from admin and will be shortly removed add added to
    #RedEmailList or GreenEmailList.
    #"""
    #extension = models.CharField(max_length=30)
    #objects = EmailListManager()

    def __unicode__(self):
        return u"%s" % self.extension


class AdminVerifyStudentEmailListManager(models.Manager):
    """
    Defines the table level methods for AdminVerifyEmailList.
    """
    def add(self, user, email):
        self.create(user=user, email=email)

        

class AdminVerifyStudentEmailList(models.Model):
    """
    Same use that of YellowEmailList, Contents of this list will be listed in
    admin console for verification. The emails are stored in this list. Right
    after the admin verification a verification url will be send to the email
    using the StudentEmailVerficationProfile.
    """
    user = models.ForeignKey(User, unique=True)
    email = models.CharField(max_length=75)
    college = models.CharField(max_length=300, blank=True)
    month_of_passing = models.CharField(max_length=15, blank=True)
    year_of_passing = models.IntegerField(max_length=4, null=True, blank=True)
    # Determines if mail should be send. Such a situation arises when an user
    # opts his already verified (ownership) email in database for student
    # verification.
    #send_email = models.BooleanField(default=True)
    # Instead checks if an EmailProfile exist for this mail
    objects = AdminVerifyStudentEmailListManager()

    def move_to_greenlist(self):
        """
        When admin accepts it as student email, this should be called.
        """
        #if self.send_email:
            #verification_profile = \
                    #EmailVerificationProfile.objects.create_profile(
                                                    #email=self.email,
                                                    #user=self.user)
            #verification_profile.send_student_verification_email()
        #else:
            ## No need to send email means, it is the verified email in the
            ## database and it is student email. So it is copied to
            ## student_email field in user profile.
            #user_profile = self.user.get_profile()
            #user_profile.student_email = self.email
            #user_profile.save()
        user_profile = self.user.get_profile()
        user_profile.student_email = self.email
        user_profile.confirm_as_student()
        user_profile.save()
        send_student_verification_success_mail(user=self.user)
        GreenEmailList.objects.add_extension_of(self.email)
        self.delete()

    def move_to_redlist(self):
        """
        When admin rejects it as a student email, this method should be called.
        """
        RedEmailList.objects.add_extension_of(self.email)
        user_profile = self.user.get_profile()
        user_profile.verify_later() # Same as that of not verified
        self.delete()

    def __unicode__(self):
        return u"%s" % self.email


class AdminVerifyEmailListManager(models.Manager):
    """
    Defines the table level methods for AdminVerifyEmailList.
    """
    def add(self, email):
        self.get_or_create(email=email)

    def refresh(self):
        """
        Verifies the extension of existing objects and moves it into green list
        and redlist.
        """
        for obj in self.all():
            if (GreenEmailList.objects.contains_extension_of(obj.email) or
                    RedEmailList.objects.contains_extension_of(obj.email)):
                obj.delete()


class AdminVerifyEmailList(models.Model):
    """
    Stores the list of emails that needs admin verification to determine if
    they should be in RedEmailList or GreenEmailList.
    """
    email = models.CharField(max_length=75)
    objects = AdminVerifyEmailListManager()

    def move_to_greenlist(self):
        """
        When admin accepts it, move to greenlist.
        """
        GreenEmailList.objects.add_extension_of(self.email)
        self.delete()

    def move_to_redlist(self):
        """
        When admin rejects it, move to redlist.
        """
        RedEmailList.objects.add_extension_of(self.email)
        self.delete()

    def __unicode__(self):
        return u"%s" % self.email


class IdCardVerificationProfile(models.Model):
    """
    This model will be used to store the id card images of the student who opts
    for verification by uploading scanned id card image.
    """
    user = models.ForeignKey(User, unique=True)
    college = models.CharField(max_length=300, blank=True)
    month_of_passing = models.CharField(max_length=15, blank=True)
    year_of_passing = models.IntegerField(max_length=4, null=True, blank=True)
    college_id_card = models.ImageField(upload_to='id_card/%y/%m/%d/%H/%M/%S',
                                        blank=True)
    other_id_document = models.ImageField(
                            upload_to='other_id_card/%y/%m/%d/%H/%M/%S/',
                            blank=True)

    def approve_profile(self):
        user_profile = self.user.get_profile()
        user_profile.college_id_card = self.college_id_card
        user_profile.other_id_document = self.other_id_document
        user_profile.confirm_as_student()
        self.delete()

    def reject_profile(self):
        user_profile = self.user.get_profile()
        user_profile.verify_later() # Marks student as not verified
        self.delete()

class CollegeListManager(models.Manager):

    def add(self, name=None):
        try:
            self.get(name=name)
        except self.model.DoesNotExist:
            self.create(name=name)

class CollegeList(models.Model):
    """
    Keeps the list of colleges in the database. Used for suggestions.
    """
    name = models.CharField(max_length=300,
                            unique=True)
    objects = CollegeListManager()

    def __unicode__(self):
        return u'%s' % self.name


class ForgotPasswordManager(models.Manager):
    """
    Defines table level methods needed for ForgotPasswordProfile.
    """

    def get_profile_from_key(self, key):
        """
        Returns the corresponding profile instance if a valid activation_key is
        given. Otherwise returns False.
        """
        if SHA1_RE.search(key):
            try:
                profile = self.get(key=key)
            except self.model.DoesNotExist:
                return False
            return profile
        return False

    def create_profile(self, user):
        """
        Created a Registration profile, generates activation_key, stores the 
        activation_key and email in the profile instance and returns it.
        """
        key = hashlib.sha1(str(random.random())).hexdigest()
        try:
            existing_profile = self.get(user=user)
            existing_profile.delete()
        except self.model.DoesNotExist:
            pass
        profile = self.create(user=user, key=key)
        profile.save()
        return profile
        

class ForgotPasswordProfile(models.Model):
    """
    This model will be used to keep data of user requested for forgot
    password option.
    """
    user = models.ForeignKey(User, unique=True)
    key = models.CharField(max_length=40)
    objects = ForgotPasswordManager()

    def send_email(self):
        """
        Sends the email to user with url to reset password.
        """
        email = self.user.get_profile().get_primary_email()
        send_forgot_password_mail(key=self.key, to_email=email)


class SocialAuthDump(models.Model):
    """
    Dumps social auth data for debugging purpose
    """
    user = models.ForeignKey(User)
    backend = models.CharField(max_length=32)
    data = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s in %s' % (self.user.username, self.backend)
