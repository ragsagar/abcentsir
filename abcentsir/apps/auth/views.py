"""
Contains views for sending student email invitation, activation, registration
and verification.
"""
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.utils import simplejson

from abcentsir.apps.auth.models import (EmailRegistrationProfile,
    GreenEmailList, RedEmailList, EmailVerificationProfile,
    StudentProfile, CollegeList, StudentEmailVerificationProfile,
    AdminVerifyEmailList, AdminVerifyStudentEmailList,
    ForgotPasswordProfile)
from abcentsir.apps.auth.forms import (EmailInviteForm, UserLoginForm,
    BasicRegistrationForm, UserProfileForm, ForgotPasswordForm,
    EmailBasedStudentVerificationForm, IdCardBasedVerificationForm,
    ChangePasswordForm, ProfileImageForm)
from abcentsir.libs import send_student_verification_success_mail
from abcentsir.apps.auth.helpers import get_username

#from django_facebook.decorators import facebook_required
#from django_facebook.api import get_persistent_graph

LOGIN_URL = reverse_lazy('auth_login_user')

def home(request):
    """
    Renders the landing page.
    """
    return render(request, 'home.html')


def request_invite(request):
    """
    Renders the form to register email for invitation. If a POST request is
    given then email if processed and scheduled for sending invitation.
    """
    if request.user.is_authenticated():
        messages.add_message(request,
                             messages.WARNING,
                             'You are already registered with AbCent Sir')
        return HttpResponseRedirect(reverse_lazy('home'))
    if request.method == 'POST':
        form = EmailInviteForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            profile = EmailRegistrationProfile.objects.create_profile(
                                                                email=email
                                                                     )
            if GreenEmailList.objects.contains_extension_of(email):
                # It is a student email. Send an invitation instantly.
                profile.send_invitation_email()

            elif RedEmailList.objects.contains_extension_of(email):
                # Not a student email. Schedule to send invitation later.
                profile.send_invitation_email_later()
            else:
                # Unknow email extensions. Send to admin for verification.
                # After verification extension of this email will be added to
                # GreenEmailList or RedEmailList
                profile.send_invitation_email_later()
                AdminVerifyEmailList.objects.add(email=email)
            messages.add_message(request, messages.SUCCESS,
                                '<strong>Yay !</strong> Thumbs Up to your Request.  You made my Day !')
            return render(request,
                          'invite_success.html')
    else:
        form = EmailInviteForm()
    return render_to_response('request_invite.html',
                              {'form': form},
                              context_instance=RequestContext(request)
                              )


def activate_account(request, activation_key):
    """
    Checks if the activation key got as argument is valid. Renders the
    Registration Form, Verification Form etc as needed.
    """
    profile = EmailRegistrationProfile.objects.get_profile_from_key(
                                                            activation_key
                                                                        )  
    if not profile:
        raise Http404
    if request.method == 'POST':
        form = BasicRegistrationForm(request.POST, email=profile.email)
        if form.is_valid():
            user = form.save()
            # To automatically login the created user to the current session
            user.username = profile.email
            user.save()
            user = authenticate(username=profile.email,
                                password=form.cleaned_data['password1'])
            login(request, user)
            profile.delete()
            # To Do . Instead of deleting we should show already activated msg
            return HttpResponseRedirect(
                            reverse_lazy('auth_verify_as_student'))
    else:
        form = BasicRegistrationForm(email=profile.email)
        request.session['activation_key'] = profile.activation_key
        request.session['invited_email'] = profile.email
    return render_to_response('auth/basic_registration.html',
                              {'form': form},
                              context_instance=RequestContext(request))


def login_user(request):
    """
    View to log the user in.
    """
    error = ''
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse_lazy('home'))
    if request.method == 'POST':
        if 'forgot_password' in request.POST:
            return HttpResponseRedirect(reverse_lazy('auth_forgot_password'))
        else:
            form = UserLoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(username=username, password=password)
                # Doing the hack to login using other attributes
                if not user:
                    user_name = get_username(username=username,
                                            password=password)
                    if user_name:
                        user = authenticate(username=user_name,
                                            password=password)
                if user and user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse_lazy('home'))
                else:
                    error = 'Invalid Login'
    else:
        form = UserLoginForm()
    return render_to_response('auth/login.html',
            {'form': form, 'error': error},
            context_instance=RequestContext(request))


def logout_user(request):
    """
    Logs-out the user and redirects to login page.
    """
    logout(request)
    return HttpResponseRedirect(reverse_lazy('auth_login_user'))


@login_required(login_url=LOGIN_URL)
def edit_user_profile_redirect(request):
    """
    Fetches the current user's id and redirect to the profile page.
    """
    return HttpResponseRedirect(
            reverse_lazy('auth_edit_user_profile',
                         kwargs={'userid': request.user.id}))


@login_required(login_url=LOGIN_URL)
def edit_user_profile(request, userid):
    """
    Renders the detailed registration form with options to connect with
    facebook.
    """
    try:
        user = User.objects.get(id=userid)
    except User.DoesNotExist:
        raise Http404
    if request.user != user and not request.user.is_staff:
        raise Http404
    if request.method == 'POST':
        if 'save_changes' in request.POST:
            image_form = ProfileImageForm(user=user)
            form = UserProfileForm(request.POST, user=user)
            if form.is_valid():
                user = form.save()
                messages.add_message(request,
                                     messages.SUCCESS,
                                     'Profile updated successfully!')
                return HttpResponseRedirect(
                            reverse_lazy('auth_edit_user_profile_redirect'))
        elif 'upload_image' in request.POST:
            form = UserProfileForm(user=user)
            image_form = ProfileImageForm(request.POST,
                                          request.FILES,
                                          user=user)
            if image_form.is_valid():
                image_form.save()
                messages.add_message(request,
                                     messages.SUCCESS,
                                     'Changed Profiled Image')
                return HttpResponseRedirect(
                                reverse_lazy('auth_edit_user_profile_redirect'))
        elif 'verify' in request.POST:
            form = UserProfileForm(request.POST, user=user)
            if form.is_valid():
                user = form.save()
            return HttpResponseRedirect(reverse_lazy('auth_verify_as_student'))
    else:
        user_profile = user.get_profile()
        if user_profile.verified_as_student:
            status = 0
        elif user_profile.not_student:
            status = 2
        else:
            status = 1
        initial_data = {'primary_email': user.username,
                        'first_name': user.first_name,
                        'last_name': user.last_name,
                        'username': user_profile.user_name,
                        'email': user.email,
                        #'address': user_profile.address,
                        'mobile_number': user_profile.mobile_number,
                        'college': user_profile.college,
                        'alternate_email': user_profile.registered_email,
                        'use_alternate_email': 
                            user_profile.use_registered_email,
                        'facebook_share': user_profile.facebook_share,
                        'status': status,
                        }
        form = UserProfileForm(initial=initial_data, user=user)
        image_form = ProfileImageForm(user=user)
    return render_to_response('auth/edit_user_profile.html',
                    {'form': form, 'user_profile': user.get_profile(),
                     'image_form': image_form},
                    context_instance=RequestContext(request))


@login_required(login_url=LOGIN_URL)
def verify_as_student(request):
    """
    Renders the form to upload college identity card and other documents or a
    form to enter the college email id.
    """
    user_profile = request.user.get_profile()
    if user_profile.verified_as_student:
        messages.add_message(request,
                             messages.INFO,
                             'You are already verified as student')
        return HttpResponseRedirect(reverse_lazy('home'))
    elif user_profile.applied_for_verification:
        messages.add_message(request,
                             messages.INFO,
                             'Your student verification is pending for admin approval')
        return HttpResponseRedirect(reverse_lazy('home'))

    if request.method == 'POST':
        skip_message = " <strong>Thanks for registering!</strong> Please consider " + \
                       "completing your <a href='%s'>profile</a>" % (
                       reverse_lazy('auth_edit_user_profile',
                                    kwargs={'userid': request.user.id}))
        if 'email_verification' in request.POST:
            id_card_form = IdCardBasedVerificationForm()
            email_form = EmailBasedStudentVerificationForm(request.POST,
                                                           user=request.user)
            if email_form.is_valid():
                # Message to be displayed is found in forms save() and returned
                msg = email_form.save()
                messages.add_message(request,
                                     messages.SUCCESS,
                                     msg)
                return HttpResponseRedirect(reverse_lazy('home'))
        elif 'id_card_verification' in request.POST:
            email_form = EmailBasedStudentVerificationForm()
            id_card_form = IdCardBasedVerificationForm(request.POST,
                                                       request.FILES,
                                                       user=request.user)
            if id_card_form.is_valid():
                id_card_form.save()
                user_profile.wait_for_confirmation()
                # To change this message at a later part
                msg = skip_message
                messages.add_message(request,
                                    messages.SUCCESS,
                                    msg)
                return HttpResponseRedirect(reverse_lazy('home'))
        elif 'verify_later' in request.POST:
            # Flags should be reset to reflect as student who opts to verify
            # at later point
            user_profile.verify_later()
            messages.add_message(request,
                         messages.SUCCESS,
                         skip_message)
            return HttpResponseRedirect(reverse_lazy('home'))
        elif 'not_student' in request.POST:
            user_profile.confirm_as_not_student()
            messages.add_message(request,
                         messages.SUCCESS,
                         skip_message)
            return HttpResponseRedirect(reverse_lazy('home'))
    else:
        #skip = request.GET.get('skip', None)
        #if skip == 'True':
            #return HttpResponse(reverse_lazy('home'))
        if GreenEmailList.objects.contains_extension_of(request.user.username):
            email_form = EmailBasedStudentVerificationForm(
                                    initial={'email': request.user.username,
                                             'email2': request.user.username})
        else:
            email_form = EmailBasedStudentVerificationForm()
        id_card_form = IdCardBasedVerificationForm()
    return render(request, 'auth/student_verification_form.html',
                  {'email_form': email_form, 'id_card_form': id_card_form})


def verify_student_email(request, verification_key):
    """
    Executed when the user clicks the verification url send him in the mail he
    gave during registration. Checks if the activation key has a profile and
    activates the email. Rest of the communication will be done through this
    email address.
    """
    profile = StudentEmailVerificationProfile.objects.get_profile_from_key(
                                                        verification_key)
    if profile:
        # email verified successfully. Here we are copying the current email id
        # to the student email and setting the verified email as current email
        user = profile.user
        user_profile = user.get_profile()
        #user_profile.invited_email = user.email
        #user.email = profile.email
        if GreenEmailList.objects.contains_extension_of(profile.email):
            user_profile.student_email = profile.email
            user_profile.verified_as_student = True
            user_profile.save()
            send_student_verification_success_mail(user=user)
        elif RedEmailList.objects.contains_extension_of(profile.email):
            messages.add_message(request, messages.WARNING,
                    'This mail id cannot be used for student verification')
            return HttpResponseRedirect(reverse_lazy('home'))
        else:
            AdminVerifyStudentEmailList.objects.create(
                    user=profile.user,
                    college=profile.college,
                    month_of_passing=profile.month_of_passing,
                    year_of_passing=profile.year_of_passing,
                    email=profile.email)
        profile.delete()
    else:
        raise Http404
    # Instead of render_to_response, saves some typing.
    #return render(request,
                  #'auth/email_verification_success.html',
                  #{'email': profile.email })
    messages.add_message(request,
                         messages.SUCCESS,
                         'Your email has been successfully verified.')
    return HttpResponseRedirect(reverse_lazy('home'))


def verify_alternate_email(request, verification_key):
    """
    Does normal email verification. Retrieves the email profile from the
    database and its user. then that user's registered email is set as the
    email profile's email.
    """
    email_profile = EmailVerificationProfile.objects.get_profile_from_key(
                                                         verification_key)
    if email_profile:
        user_profile = email_profile.user.get_profile()
        user_profile.registered_email = email_profile.email
        user_profile.save()
        email_profile.delete()
    else:
        raise Http404
    messages.add_message(request,
                         messages.SUCCESS,
                         'Your email has been verified successfully.')
    return HttpResponseRedirect(reverse_lazy('auth_edit_user_profile_redirect'))

        

#@facebook_required(scope='publish_actions')  
#def open_graph_beta(request):  
    #''''' 
    #Simple example view on how to do open graph postings 
    #'''  
    #fb = get_persistent_graph(request)  
    #entity_url = 'http://thawing-lake-6984.herokuapp.com/fbresponse/'  
    #result = fb.set('me/liberlabstest:interest', item=entity_url)  


def get_colleges(request):
    """
    Returns a list of college names as json.
    """
    college_list = [college.name for college in CollegeList.objects.all()]
    college_list = simplejson.dumps(college_list)
    return HttpResponse(college_list, mimetype='application/json')


def forgot_password(request):
    """
    A ForgotPasswordProfile object will be created and a link will be send to
    the user's mail.
    """
    if request.method == 'POST':
        form = ForgotPasswordForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request,
                                 messages.INFO,
                                 "Check your mail for a one time password reset"
                                 " link")
            return HttpResponseRedirect(reverse_lazy('home'))
    else:
        form = ForgotPasswordForm()
    return render(request,
                  'auth/forgot_password.html',
                  {'form': form})


def onetime_change_password(request, key):
    """
    Allows the user to change password once. After finding and validating
    the key, the user will be automatically signed in and redirected to a
    change password form.
    """
    profile = ForgotPasswordProfile.objects.get_profile_from_key(key=key)
    if profile:
        user = profile.user
        profile.delete()
    else:
        raise Http404
    # A small hack to log the user in
    # login() function expects a 'backend' attribute set by authenticate() call
    # in the user object. So we are manually setting it to imitate
    # authentication
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, user)
    return HttpResponseRedirect(reverse_lazy('auth_change_password',
                                kwargs={'userid': user.id}))


@login_required(login_url=LOGIN_URL)
def change_password(request, userid):
    """
    Displays a form for changing the password of the user.
    Old password won't be asked.
    """
    try:
        user = User.objects.get(id=userid)
    except User.DoesNotExist:
        raise Http404
    if request.user != user and not request.user.is_staff:
        print "Permission Denied"
        raise Http404
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            user = form.save(user=user)
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Password Changed Successfully.')
            return HttpResponseRedirect(reverse_lazy('auth_edit_user_profile',
                                        kwargs={'userid': user.id}))
    else:
        form = ChangePasswordForm()
    return render(request,
                  'auth/change_password.html',
                  {'form': form})

