from abcentsir.apps.auth.models import StudentProfile

def get_username(username, password):
    """
    Authenticates the user and returns user object using the registered email
    and user_name fields in StudentProfile.
    """
    if '@' in username:
        try:
            user_profile = \
                    StudentProfile.objects.get(registered_email=username)
        except:
            # Returns None even if there are multiple objects
            return None
    else:
        try:
            user_profile = StudentProfile.objects.get(user_name=username)
        except:
            return None
    user = user_profile.user
    return user
