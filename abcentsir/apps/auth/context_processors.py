

def username_generator(request):
    """
    Adds the {{ username }} to template. which can be used to display the
    username.
    """
    user = request.user
    username = ''
    if user.is_authenticated():
        user_profile = user.get_profile()
        if user_profile.user_name:
            username = user_profile.user_name
        else:
            username = user.username.split('@')[0]
    return {'username': username}
