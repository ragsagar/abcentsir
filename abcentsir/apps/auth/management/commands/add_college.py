from django.core.management.base import BaseCommand, CommandError
import xlrd
from abcentsir.apps.auth.models import CollegeList


class Command(BaseCommand):
    args = 'college_list.xls'
    help = 'Give list of college names as excel file'

    def handle(self, fname, **options):
        if not fname:
            CommandError("Filename required.")
        book = xlrd.open_workbook(fname)
        sheet = book.sheets()[0]
        for row in range(sheet.nrows):
            CollegeList.objects.add(name=sheet.row_values(row)[0])
        print "%d colleges added to database." % sheet.nrows

        

