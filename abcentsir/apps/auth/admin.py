from django.contrib import admin
from abcentsir.apps.auth.models import GreenEmailList, RedEmailList
from abcentsir.apps.auth.models import (AdminVerifyStudentEmailList,
        AdminVerifyEmailList)
from abcentsir.apps.auth.models import SocialAuthDump


class GreenListAdmin(admin.ModelAdmin):
    pass

class RedListAdmin(admin.ModelAdmin):
    pass

class AdminVerifyEmailListAdmin(admin.ModelAdmin):
    pass

class AdminVerifyStudentEmailListAdmin(admin.ModelAdmin):
    pass

class SocialAuthDumpAdmin(admin.ModelAdmin):
    pass


admin.site.register(GreenEmailList, GreenListAdmin)
admin.site.register(RedEmailList, RedListAdmin)
admin.site.register(AdminVerifyEmailList, AdminVerifyEmailListAdmin)
admin.site.register(AdminVerifyStudentEmailList,
        AdminVerifyStudentEmailListAdmin)
admin.site.register(SocialAuthDump, SocialAuthDumpAdmin)
