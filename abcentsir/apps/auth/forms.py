import calendar
from django import forms
from django.conf import settings
from PIL import Image
from os.path import join
from datetime import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Layout, Submit, Div, Field, Fieldset, HTML, MultiField

from abcentsir.apps.auth.models import User, EmailVerificationProfile
from abcentsir.apps.auth.models import StudentEmailVerificationProfile
from abcentsir.apps.auth.models import GreenEmailList, RedEmailList
from abcentsir.apps.auth.models import AdminVerifyStudentEmailList
from abcentsir.apps.auth.models import AdminVerifyEmailList
from abcentsir.apps.auth.models import IdCardVerificationProfile
from abcentsir.apps.auth.models import CollegeList, StudentProfile
from abcentsir.apps.auth.models import ForgotPasswordProfile
from abcentsir.libs import send_student_verification_success_mail



class EmailInviteForm(forms.Form):
    """
    The form in the landing page where a student can enter his/her email id to
    recieve invitation.
    """
    email = forms.EmailField(required=True,
                             label='Email',
                             max_length=30,
                             help_text='Proxy, Not Allowed ;)')

    def clean_email(self):
        """
        If there is already a registered username that is same as that of this
        email id, a validation error will be raised.
        """
        if User.objects.filter(username=self.cleaned_data['email']):
            raise forms.ValidationError('You are already registered with '
                                        'AbCentSir')
        if StudentProfile.objects.filter(
                registered_email=self.cleaned_data['email']):
            raise forms.ValidationError('You are already registered with '
                                        'AbCentSir')
        return self.cleaned_data['email']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'Hey Lucky You ! Just <span style="color:red;">ONE</span> seat Left. Come on In ;)',
                    Field('email', placeholder='email (student email preferably)'),
                    FormActions(
                    Submit('request', 'Request', css_class='btn-danger')
                    )
                    ),
                )
        super(EmailInviteForm, self).__init__(*args, **kwargs)


class UserLoginForm(forms.Form):
    """
    Fields needed to render login page.
    """
    username = forms.CharField(label='Email/Username',
                               required=True)
    password = forms.CharField(widget=forms.PasswordInput(render_value=False),
                               label='Password',
                               required=True)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_action = ''
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
                Fieldset(
                    'Login using AbCent Sir account',
                    Field('username', css_class='input-xlarge'),
                    Field('password', css_class='input-xlarge'),
                    FormActions(
                        Submit('login', 'Login', css_class='btn-info'),
                        Submit('forgot_password', 'Forgot Password')
                        ),
                    HTML('<input type="hidden" name="next" value="{{ next }}" />'),
                ),
                )
        super(UserLoginForm, self).__init__(*args, **kwargs)


class BasicRegistrationForm(forms.Form):
    """
    Field required in the student registration form.
    """
    #email = forms.CharField(label='Email',
                            #required=True)
    password1 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='Password',
                                required=True,
                                min_length=6,
                                help_text='Make sure you keep it safe!')
    password2 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='Password (Again)',
                                required=True,
                                min_length=6,
                                help_text="Just checking your typing skills")

    def __init__(self, *args, **kwargs):
        self.email = kwargs.pop('email', None)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'Create an Account',
                    HTML('<div class="control-group"><label class="control-label">Email</label><div class="controls"><span class="input-xlarge uneditable-input">%s</span><p class="help-block">we already knew it ;)</p></div></div>' % self.email),
                    Field('password1', css_class='input-xlarge'),
                    Field('password2', css_class='input-xlarge'),
                    HTML('<div class="control-group"><div class="controls"><label class="checkbox"><input type="checkbox" id="ToSCB" value="option1">I accept <a href="#ToS" data-toggle="modal">Terms and Conditions</a> and  <a href="#Privacy" data-toggle="modal">Privacy Policy</a> of this website.</label></div></div>'),
                    FormActions(
                        Submit('create_account', 'Create Account',
                               css_class='btn-info btn-large',disabled='disabled'),
                                )
                        ),
                )
        super(BasicRegistrationForm, self).__init__(*args, **kwargs)

    #def clean_email(self):
        #"""
        #Checks if the username is already taken and raises ValidationError.
        #"""
        #if User.objects.filter(username=self.cleaned_data['email']):
            #raise forms.ValidationError('Email already exists.')
        #return self.cleaned_data['email']

    def clean(self):
        """
        Checks if both passwords entered are same. Raises ValidationError if it
        is not.
        """
        if 'password1' in self.cleaned_data and \
            'password2' in self.cleaned_data:
                if self.cleaned_data['password1'] != \
                   self.cleaned_data['password2']:
                    raise forms.ValidationError('Entered passwords are not'
                                                ' same')
        return self.cleaned_data

    def save(self):
        """
        Saves the user input data into the dataabase.
        """
        user = User.objects.create_user(username=self.email,
                                        password=self.cleaned_data['password1'])
        user.is_active = True
        user.save()
        return user


class UserProfileForm(forms.Form):
    """
    Form rendered for the detailed registration. None of the fields will be
    mandatory.
    """
    STATUS_CHOICES = (
            (0, 'Student'),
            (1, 'Student (Unverified)'),
            (2, 'Not a student'))
    primary_email = forms.CharField(label='Primary Email',
                                    required=False)
    first_name = forms.CharField(label='First Name',
                                 required=False,
                                 help_text="Sorry, no clues for this")
    last_name = forms.CharField(label='Last Name',
                                 required=False,
                                 help_text='Guess this will be your family '
                                 'name!')
    username = forms.RegexField(regex=r'^[a-zA-Z0-9_]+$',
                                label='Username',
                                required=False,
                                min_length=4,
                                error_messages={'invalid': 'Please use only '
                                'letter, numbers and underscore'},
                                help_text='Your pet name, if you please')
    #address = forms.CharField(widget=forms.Textarea,
                              #label='Address',
                              #required=False,
                              #help_text='You still remember your own snail '
                              #' mail address')
    college = forms.CharField(label='Univeristy/Institute',
                              required=False,
                              help_text='You probably will avoid this from'
                              ' here onwards!')
    alternate_email = forms.CharField(label='Alternate Email',
                                      max_length=75,
                                      required=False,
                                      help_text='Your alternate email address')
    use_alternate_email = forms.BooleanField(label='Use as primary mail'
                                                   ' address',
                                             help_text='Select this to use '
                                                       'alternate email for '
                                                       'communication',
                                             required=False)
    mobile_number = forms.IntegerField(label='Mobile Number',
                                       required=False,
                                       help_text='Lets text sometimes ;)')
    facebook_share = forms.BooleanField(label='Share in Facebook',
                                        help_text="Uncheck this if you don't"
                                        " want updates to be posted to "
                                        "facebook",
                                        required=False)
    status = forms.ChoiceField(label='Status',
                               choices=STATUS_CHOICES,
                               required=False)
    #profile_image = forms.ImageField(label='Profile Image',
                                     #required=False,
                                     #help_text='We know when you use photoshop')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        user_profile = self.user.get_profile()
        #if user_profile.profile_image:
            #image_path = user_profile.profile_image.url
        #else:
            #image_path = ''
        # To turn the toggle button on if facebook_share is true
        if user_profile.facebook_share:
            checked = "checked='checked'"
        else:
            checked = ""
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'My Beautiful Profile',
                Field('primary_email', css_class='input-xlarge',
                      readonly=True),
                Field('alternate_email', css_class='input-xlarge'),
                Field('use_alternate_email', css_class='input-xlarge'),
                Field('first_name', css_class='input-xlarge'),
                Field('last_name', css_class='input-xlarge'),
                Field('username', css_class='input-xlarge'),
                #Field('address', rows=3, css_class='input-xlarge'),
                Field('college', css_class='input-xlarge',
                      readonly=True),
                Field('mobile_number', css_class='input-xlarge'),
                HTML('<div class="control-group"><label for="optionsCheckbox" class="control-label">Share in Facebook</label><div class="controls"><div id="share-toggle-button"><input id="checkbox1" type="checkbox" value="value1" %s name="facebook_share"></div></div></div>' % checked),
                Field('status', css_class='input-large',disabled="disabled"),
                #Field('profile_image', css_class='input-file'),
                #HTML('<div class="profile_img"><img width="160" height="160" src="%s" /></div>' % image_path),
                FormActions(
                    Submit('save_changes', 'Save Changes',
                            css_class='btn-info btn-large'),
                    )
                ),
                )
        super(UserProfileForm, self).__init__(*args, **kwargs)
        if EmailVerificationProfile.objects.filter(user=self.user):
            email_profile = EmailVerificationProfile.objects.get(user=self.user)
            message = '%s Email confirmation is <span style="color:red;">pending.</span>' % email_profile.email
            self.fields['alternate_email'].help_text = message
        if not user_profile.registered_email:
            self.fields['use_alternate_email'].widget.attrs['disabled'] = \
                    'disabled'
        if user_profile.user_name:
            self.fields['username'].widget.attrs['readonly'] = True

    def clean_username(self):
        if self.user.get_profile().user_name != self.cleaned_data['username']:
            if StudentProfile.objects.filter(
                        user_name=self.cleaned_data['username']):
                raise forms.ValidationError('Already exists, try another')
            if User.objects.filter(username=self.cleaned_data['username']):
                raise forms.ValidationError('Already exists, try another')
        return self.cleaned_data['username']

    def clean_alternate_email(self):
        if self.cleaned_data['alternate_email'] == self.user.username:
            registered_email = self.user.get_profile().registered_email
            self.cleaned_data['alternate_email'] = registered_email
        return self.cleaned_data['alternate_email']

    def save(self):
        """
        Saves the detailed user info to the database.
        """
        user_profile = self.user.get_profile()
        self.user.first_name = self.cleaned_data['first_name']
        self.user.last_name = self.cleaned_data['last_name']
        if not user_profile.user_name:
            user_profile.user_name = self.cleaned_data['username']
        if (user_profile.registered_email !=
                self.cleaned_data['alternate_email']):
            if self.cleaned_data['alternate_email']:
                if EmailVerificationProfile.objects.filter(user=self.user):
                    e = EmailVerificationProfile.objects.get(user=self.user)
                    e.delete()
                verification_profile = \
                        EmailVerificationProfile.objects.create_profile(
                                            user=self.user,
                                            email=self.cleaned_data['alternate_email'])
                verification_profile.send_verification_email()
            else:
                user_profile.registered_email = self.cleaned_data[
                                                        'alternate_email']
        #user_profile.address = self.cleaned_data['address']
        #user_profile.college = self.cleaned_data['college']
        # Adding the college to CollegeList model
        #CollegeList.objects.add(name=self.cleaned_data['college'])
        mobile_number = self.cleaned_data['mobile_number']
        if mobile_number:
            user_profile.mobile_number = mobile_number
        #user_profile.profile_image = self.cleaned_data['profile_image']
        if user_profile.registered_email:
            user_profile.use_registered_email = \
                    self.cleaned_data['use_alternate_email']
        user_profile.facebook_share = self.cleaned_data['facebook_share']
        user_profile.save()
        self.user.save()
        #if user_profile.profile_image:
            #image_filename = user_profile.profile_image._get_path()
            #image = Image.open(image_filename)
            #image.thumbnail((160,160), Image.ANTIALIAS)
            #image.save(image_filename, 'PNG')
        #return self.cleaned_data['status'] # To redirect based on the status


class ProfileImageForm(forms.Form):
    """
    Form used to upload profile images.
    """
    profile_image = forms.ImageField(label='',
                                     required=True)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        user_profile = self.user.get_profile()
        if user_profile.profile_image:
            image_path = user_profile.profile_image.url
        else:
            image_path = '{{ STATIC_URL }}img/profile_image.png'
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
            Fieldset('Change Profile Image',
                HTML('<div class="profile_img"><img src="%s" /></div>' % image_path),
                Field('profile_image', css_class='input-file',template="field_nolabel.html"),
                Submit('upload_image', 'Upload',css_class='btn btn-large'),
                ),
                )
        super(ProfileImageForm, self).__init__(*args, **kwargs)


    def save(self):
        user_profile = self.user.get_profile()
        user_profile.profile_image = self.cleaned_data['profile_image']
        user_profile.save()
        if user_profile.profile_image:
            basewidth = 160
            image_filename = user_profile.profile_image._get_path()
            image = Image.open(image_filename)
            wpercent = (basewidth/float(image.size[0]))
            hsize = int((float(image.size[1])*float(wpercent)))
            image = image.resize((basewidth,hsize), Image.ANTIALIAS)
            image.save(image_filename, 'PNG')



class EmailBasedStudentVerificationForm(forms.Form):
    """
    Form used to verify as a student using email.
    Both field will be marked as not required. But there should be frontend
    validation to make sure that only one value is entered.
    """
    #same_email = forms.BooleanField(label='Use the same email I used'
                                    #' while creating the account',
                                    #required=False,
                                    #help_text="You won't get verified"
                                    #" using public email service"
                                    #" like gmail, yahoo or rediff.")
    year = datetime.now().year
    YEAR_CHOICES = [(y, str(y)) for y in range(year+1, year+6)]
    MONTH_CHOICES = [(month, month) for month in calendar.month_name if month]
    email = forms.EmailField(label='Student Email',
                             required=True,
                             help_text='Make sure this is your valid student email id')
    email2 = forms.EmailField(label='Student Email(Again)',
                              required=True,
                              help_text='This is to make sure you haven\'t made a typo')
    college = forms.CharField(label='University/Institute',
                              required=True)
    month_of_passing = forms.ChoiceField(choices=MONTH_CHOICES,
                                         label='Month of passing',
                                         required=True,
                                         help_text="Month of passing")
    year_of_passing = forms.ChoiceField(choices=YEAR_CHOICES,
                                        label='Year of passing',
                                        required=True,
                                        help_text='Year of Passing')

    def __init__(self, *args, **kwargs):
        """
        Initialized the self.user to current user.
        """
        self.user = kwargs.pop('user', None)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'Verify using my student email id',
                    Field('email', css_class='input-xlarge',
                          placeholder='email@college.xxx'),
                    Field('email2', css_class='input-xlarge',
                          placeholder='email@college.xxx'),
                    Field('college', css_class='input-xlarge college',
                          autocomplete='off'),
                    HTML(' <div class="control-group"><label class="control-label" for="input01">Passing Out</label><div class="controls">'),
                    Field('month_of_passing', css_class='input-medium',style='margin-right:38px;',template="field_nowrap.html"),
                    Field('year_of_passing', css_class='input-small',template="field_nowrap.html"),
                    HTML('</div></div>'),
                    HTML('<div class="control-group"><div class="controls"><label class="checkbox"><input type="checkbox" id="emailVerTosCB" value="option1">"Yes! Yes! I am a student currently" and I have read Detailed <a href="#studentToS" data-toggle="modal">Student terms and Conditions</a></label></div></div>'),
                    FormActions(
                        Submit('email_verification', 'Send verification '
                               'request', css_class='btn-info btn-large',disabled='disabled'))
                    ))
        super(EmailBasedStudentVerificationForm, self).__init__(*args, **kwargs)


    def clean(self):
        if 'email' in self.cleaned_data and 'email2' in self.cleaned_data:
            if self.cleaned_data['email'] != self.cleaned_data['email2']:
                raise forms.ValidationError('Enter the same email in both'
                                            ' fields.')
            if RedEmailList.objects.contains_extension_of(
                                            self.cleaned_data['email']):
                raise forms.ValidationError("Sorry, the email you have entered "
                        "doesn't seem to be a Valid student email. Please enter"
                        " a valid student email or Verify using your ID cards")
        return self.cleaned_data

    def save(self):
        """
        If the email is already there in the user db, then no need to send
        verification mail again. He is verified if it a student email.
        """
        message = "We have got your verification request. We will get back to " \
                  " you."
        # Updated verification saving.
        email = self.cleaned_data['email']
        user_profile = self.user.get_profile()
        if ( email != self.user.username and
             email != self.user.email and
             email != user_profile.registered_email ):
            profile = StudentEmailVerificationProfile.objects.create_profile(
                          email=email,
                          user=self.user,
                          college=self.cleaned_data['college'],
                          month_of_passing=self.cleaned_data['month_of_passing'],
                          year_of_passing=self.cleaned_data['year_of_passing'],
                                                            )
            #if not GreenEmailList.objects.contains_extension_of(email):
                ## It wont be Red list as validation will take care of it. if it
                ## is not in greenlist the push to admin for verification
                #AdminVerifyStudentEmailList.objects.create(
                        #user=self.user,
                        #college=self.cleaned_data['college'],
                        #month_of_passing=self.cleaned_data['month_of_passing'],
                        #year_of_passing=self.cleaned_data['year_of_passing'],
                        #email=email)
            message = ("We have got your verification request. Please verify"
                      " your student email. Check your student email"
                      " inbox/spam")
            profile.send_student_verification_email()
        else:
            if GreenEmailList.objects.contains_extension_of(email):
                user_profile.student_email = email
                user_profile.confirm_as_student()
                send_student_verification_success_mail(user=self.user)
                message = "You have successfully verified as student"
            else:
                AdminVerifyStudentEmailList.objects.create(
                        user=self.user,
                        college=self.cleaned_data['college'],
                        month_of_passing=self.cleaned_data['month_of_passing'],
                        year_of_passing=self.cleaned_data['year_of_passing'],
                        email=email)
                user_profile.wait_for_confirmation()
        user_profile.college = self.cleaned_data['college']
        CollegeList.objects.add(name=self.cleaned_data['college'])
        user_profile.month_of_passing = self.cleaned_data['month_of_passing']
        user_profile.year_of_passing = self.cleaned_data['year_of_passing']
        user_profile.save()
        return message



class IdCardBasedVerificationForm(forms.Form):
    """
    Form for verification by uploading id card.
    Either one should or both should be filled. Both cannot be empty which
    should be validated in frontend.

    """
    year = datetime.now().year
    YEAR_CHOICES = [(y, str(y)) for y in range(year+1, year+6)]
    MONTH_CHOICES = [(month, month) for month in calendar.month_name if month]
    college_id_card = forms.ImageField(label='Copy of your student ID card',
                        required=True,
                        help_text='Use your Camera Phone for a Quick Scan')
    other_id_document = forms.ImageField(label='Copy of a Govt. issued ID card',
                                               required=True,
                                               help_text='Voter Card | PAN | Passport | Driving License etc.')
    college = forms.CharField(label='University/Institute',
                              required=True)
    month_of_passing = forms.ChoiceField(choices=MONTH_CHOICES,
                                         label='Month of passing',
                                         required=True,
                                         help_text='Month of passing')
    year_of_passing = forms.ChoiceField(choices=YEAR_CHOICES,
                                        label='Year of passing',
                                        required=True,
                                        help_text='Year of Passing')

    def __init__(self, *args, **kwargs):
        """
        Initialized the self.user to current user.
        """
        self.user = kwargs.pop('user', None)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'Verify using my ID cards',
                    Field('college_id_card', css_class='input-file'),
                    Field('other_id_document', css_class='input-file'),
                    Field('college', css_class='input-xlarge college',
                          autocomplete='off'),
                    HTML(' <div class="control-group"><label class="control-label" for="input01">Passing Out</label><div class="controls">'),
                    Field('month_of_passing', css_class='input-medium',style='margin-right:38px;',template="field_nowrap.html"),
                    Field('year_of_passing', css_class='input-small',template="field_nowrap.html"),
                    HTML('</div></div>'),
                    HTML('<div class="control-group"><div class="controls"><label class="checkbox"><input type="checkbox" id="idVerTosCB" value="option1">"Yes! Yes! I am a student currently" and I have read Detailed <a href="#studentToS" data-toggle="modal">Student terms and Conditions</a></label></div></div>'),
                    FormActions(
                        Submit('id_card_verification', 'Send verification '
                        'request', css_class='btn-info btn-large',disabled='disabled'))
                        )
                )
        super(IdCardBasedVerificationForm, self).__init__(*args, **kwargs)

    #def clean(self):
        #"""
        #Checks if both fields are empty and raises ValidationError.
        #"""
        #if not self.cleaned_data['college_id_card'] and \
                #not self.cleaned_data['other_id_document']:
                    #raise forms.ValidationError('Both fields cannot be empty.')
        #return self.cleaned_data

    def save(self):
        """
        Saves the profile image and user instance to a profile for admin
        verification.
        """
        message = "We have got your verification request. We will get back to " \
                  " you."
        id_profile = IdCardVerificationProfile.objects.create(
                        user=self.user,
                        college=self.cleaned_data['college'],
                        month_of_passing=self.cleaned_data['month_of_passing'],
                        year_of_passing=self.cleaned_data['year_of_passing'],
                        college_id_card=self.cleaned_data['college_id_card'],
                        other_id_document=self.cleaned_data['other_id_document']
                        )
        user_profile = self.user.get_profile()
        user_profile.college = self.cleaned_data['college']
        CollegeList.objects.add(name=self.cleaned_data['college'])
        user_profile.month_of_passing = self.cleaned_data['month_of_passing']
        user_profile.year_of_passing = self.cleaned_data['year_of_passing']
        return message


class ForgotPasswordForm(forms.Form):
    """
    Renders an email field to enter the email user used.
    """
    username = forms.CharField(max_length=30,
                               label="Email/Username")

    def __init__(self, *args, **kwargs):
        """
        Initialized the self.user to current user.
        """
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.form_action = ''
        self.helper.layout = Layout(
                Fieldset('Forgot Password',
                    Field('username', css_class='input-file'),
                    FormActions(
                        Submit('Submit', 'Submit',
                               css_class='btn-info btn-large'),
                               ),)
                        )
        super(ForgotPasswordForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        error_msg = 'No such user in our database.'
        if '@' in self.cleaned_data['username']:
            if (not User.objects.filter(username=self.cleaned_data['username']) and
                not StudentProfile.objects.filter(
                    registered_email=self.cleaned_data['username'])):
                raise forms.ValidationError(error_msg)
        else:
            if not StudentProfile.objects.filter(
                            user_name=self.cleaned_data['username']):
                raise forms.ValidationError(error_msg)
        return self.cleaned_data['username']

    def save(self):
        """
        Fetches the user object and creates and ForgotPasswordProfile object
        and send the url to change password to the user.
        """
        username = self.cleaned_data['username']
        if '@' in username:
            if User.objects.filter(username=username):
                user = User.objects.get(username=username)
            else:
                user_profile = StudentProfile.objects.get(registered_email=username)
                user = user_profile.user
        else:
            user_profile = StudentProfile.objects.get(user_name=username)
            user = user_profile.user
        profile = ForgotPasswordProfile.objects.create_profile(user=user)
        profile.send_email()


class ChangePasswordForm(forms.Form):
    """
    Renders the form to change the password of the user.
    """
    password1 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='Password',
                                min_length=6,
                                required=True)
    password2 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='Password(Again)',
                                min_length=6,
                                required=True)

    def __init__(self, *args, **kwargs):
        """
        Initialized the self.user to current user.
        """
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.form_action = ''
        self.helper.layout = Layout(
                Fieldset('Change Password',
                    Field('password1', css_class='input-file'),
                    Field('password2', css_class='input-file'),
                    FormActions(
                        Submit('change_password', 'Change Password',
                               css_class='btn-info btn-large'),
                               ),)
                        )
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    #def clean_password1(self):
        #if len(self.cleaned_data['password1']) < 6:
            #raise forms.ValidationError("Minimum six characters required.")
        #return self.cleaned_data['password1']

    #def clean_password2(self):
        #if len(self.cleaned_data['password2']) < 6:
            #raise forms.ValidationError("Minimum six characters required.")
        #return self.cleaned_data['password2']

    def clean(self):
        """
        Checks if both password entered are same and raise validation error if
        they are not.
        """
        if ('password1' in self.cleaned_data and 'password2' in
                self.cleaned_data):
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError('Passwords entered are not same.')
        return self.cleaned_data

    def save(self, user):
        password = self.cleaned_data['password1']
        user.set_password(password)
        user.save()
        return user
