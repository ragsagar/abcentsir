from uuid import uuid4
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User
from django.contrib import messages

from social_auth.utils import setting
from social_auth.models import UserSocialAuth
from social_auth.backends import USERNAME
from social_auth.backends.contrib.linkedin import LinkedinBackend

from abcentsir.apps.auth.models import StudentProfile, EmailRegistrationProfile


#def stop_login_without_registration(request, *args, **kwargs):
    #"""
    #If the user tries to login fb button in login page django-social-auth will
    #create user automatically. This shouldn't be allowed as abcentsir
    #registration is only with invitation. This is dealt by this pipeline
    #function.
    #"""
    ## If the user is not logged in kwargs['user'] will be None
    ## request['activation_key'] is set in activation page. So user clicking fb
    ## button from activation page (who is not logged in) won't be stopped from
    ## connecting with fb.
    #if (not kwargs['request'].session.get('activation_key') and
            #kwargs.get('user') is None):
        #return HttpResponseRedirect(reverse_lazy('auth_login_error'))


def get_username(details, user=None,
                 user_exists=UserSocialAuth.simple_user_exists,
                 *args, **kwargs):
    """Return an username for new user. Return current user username
    if user was given.
    """
    if user:
        return {'username': user.username}
    else:
        if 'request' in kwargs:
            invited_email = kwargs['request'].session.get('invited_email')
            print invited_email
            if invited_email:
                return {'username': invited_email}

    if details.get(USERNAME):
        username = unicode(details[USERNAME])
    else:
        username = uuid4().get_hex()

    uuid_length = 16
    max_length = UserSocialAuth.username_max_length()
    short_username = username[:max_length - uuid_length]
    final_username = username[:max_length]

    # Generate a unique username for current user using username
    # as base but adding a unique hash at the end. Original
    # username is cut to avoid any field max_length.
    while user_exists(username=final_username):
        username = short_username + uuid4().get_hex()[:uuid_length]
        final_username = username[:max_length]

    return {'username': final_username}


def create_user(backend, details, response, uid, username, user=None, *args,
                **kwargs):
    """
    Create user. Depends on get_username pipeline.
    Overriding this function to disable login of unregistered users.
    """
    if isinstance(backend, LinkedinBackend):
        details.update({'email': response.get('email-address')})
    if user:
        return {'user': user}
    else:
        if ('request' in kwargs and 
                not kwargs['request'].user.is_authenticated()):
            if not 'activation_key' in kwargs['request'].session:
                email = details.get('email') or ''
                if email:
                    if User.objects.filter(username=email):
                        user = User.objects.get(username=email)
                        return {'user': user}
                    elif User.objects.filter(email=email):
                        user = User.objects.get(email=email)
                        return {'user': user}
                    elif StudentProfile.objects.filter(registered_email=email):
                        user_profile = StudentProfile.objects.get(
                                                registered_email=email)
                        return {'user': user_profile.user}
                messages.add_message(kwargs['request'], messages.WARNING,
                        'Please <b>request for invitation</b> first')
                return HttpResponseRedirect(reverse_lazy('auth_invite'))
            else:
                key = kwargs['request'].session['activation_key']
                profile = EmailRegistrationProfile.objects.get_profile_from_key(key)
                if profile:
                    profile.delete()

    if not username:
        return None
    # NOTE: not return None because Django raises exception of strip email
    email = details.get('email') or ''
    return {
        'user': UserSocialAuth.create_user(username=username, email=email),
        'is_new': True
    }

