from django.db import models
from django.contrib.auth.models import User


class EmailBasedRejectionProfile(models.Model):
    """
    Stores the comment and other details of rejected student verification
    applications.
    """
    user = models.ForeignKey(User)
    email_used = models.EmailField(max_length=75,
                                   blank=True)
    college = models.CharField(max_length=300,
                               blank=True)
    month_of_passing = models.CharField(max_length=15,
                                        blank=True)
    year_of_passing = models.IntegerField(max_length=4,
                                          blank=True,
                                          null=True)

        
class IdCardBasedRejectionProfile(models.Model):
    """
    Stores the comment and other details of the rejected id card based
    verificaiton applications.
    """
    comment = models.CharField(max_length=500,
                               blank=True)
    user = models.ForeignKey(User)
    college_id_card = models.ImageField(
                                    upload_to='rejected/id_card/%y/%m/%d/',
                                    blank=True)
    other_id_document = models.ImageField(
                                upload_to='rejected/other_docs/%y/%m/%d/',
                                blank=True)
    college = models.CharField(max_length=300,
                               blank=True)
    month_of_passing = models.CharField(max_length=15,
                                        blank=True)
    year_of_passing = models.IntegerField(max_length=4,
                                          blank=True,
                                          null=True)
