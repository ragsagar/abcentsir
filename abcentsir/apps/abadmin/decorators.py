"""
Defines the decorators needed for abadmin views.
"""
from functools import wraps
from django.conf import settings
from django.http import Http404


def admin_required(func):
    """
    Checks if the user is an admin. Raises Http404 or permission error if the
    user has is_staff set to False
    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated():
            return HttpResponseRedirect(setting.LOGIN_URL)
        if not user.is_staff or not user.is_superuser:
            print "Not admin, permission denied"
            raise Http404
        return func(request, *args, **kwargs)
    return wrapper

            
