from django import forms
from django.contrib.auth.models import User

from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Layout, Field, Submit, Fieldset

from abcentsir.apps.abadmin.models import EmailBasedRejectionProfile
from abcentsir.apps.auth.models import StudentProfile
from abcentsir.apps.offers.models import Category


class RejectionForm(forms.Form):
    """
    Form displayed when admin rejects a student verification
    application. Used for both idcard based verifications and email based
    verifications.
    """
    comment = forms.CharField(label='Comment',
                              help_text='Reason to delete',
                              required=False)


class AddAdminUserForm(forms.Form):
    """
    Form rendered to create admin users.
    """
    username = forms.CharField(label='Username',
                               required=True)
    password1 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='Password',
                                required=True)
    password2 = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                label='Password(Again)',
                                required=True)
    is_superadmin = forms.BooleanField(label='Super Admin',
                                       required=False,
                                       help_text='Super admins can create '
                                                 'new admins or superadmins.')

    def __init__(self, *args, **kwargs):
        """
        Defining the layout of crispy forms.
        """
        super(AddAdminUserForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'Create Admin User',
                    Field('username', css_class='input-xlarge'),
                    Field('password1', css_class='input-xlarge'),
                    Field('password2', css_class='input-xlarge'),
                    Field('is_superadmin', css_class='input-xlarge'),
                    FormActions(
                        Submit('create', 'Create',
                                css_class='btn-info btn-large'),
                        )
                    ),
                )
        

    def clean_username(self):
        """
        Need to verify that such an username doesn't exist anywhere as
        username, or userprofile.user_name
        """
        username = self.cleaned_data['username']
        if User.objects.filter(username=username):
            raise forms.ValidationError('Already exists in the database. '
                    'Please try another.')
        if StudentProfile.objects.filter(registered_email=username) or \
                StudentProfile.objects.filter(user_name=username):
            raise forms.ValidationError('Already exists in the database. '
                    'Please try another.')
        return username

    def clean(self):
        """
        Checks if both passwords entered are same. Raises ValidationError if it
        is not.
        """
        if 'password1' in self.cleaned_data and \
            'password2' in self.cleaned_data:
                if self.cleaned_data['password1'] != \
                   self.cleaned_data['password2']:
                    raise forms.ValidationError('Entered passwords are not'
                                                ' same')
        return self.cleaned_data

    def save(self):
        """
        Creates an user with the is_staff flag True.
        If is_superadmin is True, then is_superuser flag is also set.
        """
        username = self.cleaned_data['username']
        password = self.cleaned_data['password1']
        user = User.objects.create_user(username=username,
                                        password=password)
        user.is_staff = True
        if self.cleaned_data['is_superadmin']:
            user.is_superuser = True
        user.save()
        return user


class AddOfferForm(forms.Form):
    """
    Form for adding new offers.
    """
    category = forms.ModelChoiceField(Category)
    image = forms.ImageField(label='Offer image', required=False)
    title = forms.CharField(label='Title', required=True)
    sub_title = forms.CharField(label='Sub Title', required=False)
    scheduled_date = forms.DateTimeField(label='Scheduled Date',
                                         required=False)
    detailed_description = forms.CharField(max_length=100,
                                           required=False,
                                           label='Detailed Description')
    website_url = forms.URLField(label='Website', required=True)

    def __init__(self, *args, **kwargs):
        """
        For defining the layout of crispy forms.
        """
        super(AddOfferForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.layout = Layout(
                Fieldset(
                    'Add new offer',
                    #Field('category', css_class='input-xlarge'),
                    Field('image', css_class='input-xlarge'),
                    Field('title', css_class='input-xlarge'),
                    Field('sub_title', css_class='input-xlarge'),
                    Field('scheduled_date', css_class='input-xlarge'),
                    Field('detailed_description', css_class='input-xlarge'),
                    Field('website_url', css_class='input-xlarge'),
                    FormActions(
                        Submit('create', 'Create',
                                css_class='btn-info btn-large'),
                        )
                    ),
                )

    def save(self):
        """
        Creates the offer using the received details.
        """
        Offer.objects.create(
             category=self.cleaned_data['category'],
             image=self.cleaned_data['image'],
             title=self.cleaned_data['title'],
             sub_title=self.cleaned_data['sub_title'],
             scheduled_date=self.cleaned_data['scheduled_date'],
             detailed_description=self.cleaned_data['detailed_description'],
             website_url=self.cleaned_data['website_url'])
                         

    

