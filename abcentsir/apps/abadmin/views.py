from django.http import Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.models import User
from abcentsir.apps.auth.models import (AdminVerifyEmailList,
        AdminVerifyStudentEmailList, IdCardVerificationProfile,
        RedEmailList, GreenEmailList)
from abcentsir.apps.abadmin.forms import (RejectionForm, AddAdminUserForm,
        AddOfferForm)
from abcentsir.apps.abadmin.models import (EmailBasedRejectionProfile,
        IdCardBasedRejectionProfile)
from abcentsir.apps.abadmin.decorators import admin_required
from abcentsir.libs import (send_idcard_verification_rejection_notification,
        send_email_verification_rejection_notification)


@login_required
@admin_required
def abadmin_home(request):
    """
    Abcentsir Admin home.
    """
    return render(request,
                  'abadmin/home.html')

@login_required
@admin_required
def email_based_verification(request):
    """
    Lists the objects in AdminVerifyStudentEmailList which are the list of
    student emails pending for verification.
    """
    objects = AdminVerifyStudentEmailList.objects.all()
    return render(request,
                  'abadmin/email_based_verifications.html',
                  {'objects': objects})


@login_required
@admin_required
def approve_student_email(request, object_id):
    """
    Moves the given object to greenlist.
    """
    try:
        obj = AdminVerifyStudentEmailList.objects.get(id=object_id)
    except:
        raise Http404
    # Refresh Adminverifyemaillist after moving email extensions to redlist or
    # greenlist. So the list will get updated.
    obj.move_to_greenlist()
    AdminVerifyEmailList.objects.refresh()
    return HttpResponseRedirect(
                reverse_lazy('abadmin_email_based_verification'))


@login_required
@admin_required
def reject_student_email(request, object_id):
    """
    Moves the given object to red list.
    Also keeps track of the rejected list.
    """
    try:
        obj = AdminVerifyStudentEmailList.objects.get(id=object_id)
    except:
        raise Http404
    rejection_obj = EmailBasedRejectionProfile.objects.create(
                        user=obj.user,
                        email_used=obj.email,
                        college=obj.college,
                        year_of_passing=obj.year_of_passing,
                        month_of_passing=obj.month_of_passing)
    messages.add_message(request,
                         messages.SUCCESS,
                         'Verification Request Rejected')
    send_email_verification_rejection_notification(user=obj.user,
                                                   student_email=obj.email)
    obj.move_to_redlist()
    AdminVerifyEmailList.objects.refresh()
    return HttpResponseRedirect(
                    reverse_lazy('abadmin_email_based_verification'))


@login_required
@admin_required
def idcard_based_verification(request):
    """
    Provides interface to accept and reject student verification applications
    using id card.
    """
    objects = IdCardVerificationProfile.objects.all()
    return render(request,
                  'abadmin/id_based_verifications.html',
                  {'objects': objects})


@login_required
@admin_required
def approve_idcard(request, object_id):
    """
    Approves the idcardverificaiton object with the given object id.
    """
    try:
        obj = IdCardVerificationProfile.objects.get(id=object_id)
    except:
        raise Http404
    obj.approve_profile()
    messages.add_message(request,
                         messages.INFO,
                         "Verification request approved")
    return HttpResponseRedirect(
                reverse_lazy('abadmin_idcard_based_verification'))


@login_required
@admin_required
def reject_idcard(request, object_id):
    """
    Rejects the idcard verification object with the given object id.
    Usually run when admin rejects a student's id card verificaiton object.
    """
    try:
        obj = IdCardVerificationProfile.objects.get(id=object_id)
    except:
        raise Http404
    if request.method == 'POST':
        form = RejectionForm(request.POST)
        if form.is_valid():
            profile = IdCardBasedRejectionProfile.objects.create(
                            comment=form.cleaned_data['comment'],
                            user=obj.user,
                            college_id_card=obj.college_id_card,
                            other_id_document=obj.other_id_document,
                            college=obj.college,
                            month_of_passing=obj.month_of_passing,
                            year_of_passing=obj.year_of_passing)
            obj.reject_profile()
            send_idcard_verification_rejection_notification(profile.user,
                                                            profile.comment)
            messages.add_message(request,
                                 messages.INFO,
                                 'Verification request rejected')
    return HttpResponseRedirect(
                reverse_lazy('abadmin_idcard_based_verification'))


@login_required
@admin_required
def show_email_lists(request):
    """
    Show all green, red and yellow email lists to do approval and rejection.
    """
    yellow_list = AdminVerifyEmailList.objects.all()
    green_list = GreenEmailList.objects.all()
    red_list = RedEmailList.objects.all()
    return render(request,
            'abadmin/email_lists.html',
            {'yellow_list': yellow_list,
             'green_list': green_list,
             'red_list': red_list})


@login_required
@admin_required
def add_to_greenlist(request, object_id):
    """
    Adds the yellow list object with given object_id to green list.
    """
    try:
        obj = AdminVerifyEmailList.objects.get(id=object_id)
    except:
        return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))
    obj.move_to_greenlist()
    AdminVerifyEmailList.objects.refresh()
    messages.add_message(request,
                         messages.INFO,
                         'Extension added to greenlist.')
    return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))


@login_required
@admin_required
def add_to_redlist(request, object_id):
    """
    Adds the yellow list with given object_id to red list.
    """
    try:
        obj = AdminVerifyEmailList.objects.get(id=object_id)
    except:
        return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))
    obj.move_to_redlist()
    AdminVerifyEmailList.objects.refresh()
    messages.add_message(request,
                         messages.INFO,
                         'Extension added to redlist.')
    return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))


@login_required
@admin_required
def greenlist_to_redlist(request, object_id):
    """
    Move the domain in greenlist to redlist.
    """
    try:
        obj = GreenEmailList.objects.get(id=object_id)
    except GreenEmailList.DoesNotExist:
        return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))
    RedEmailList.objects.add(obj.extension)
    obj.delete()
    messages.add_message(request,
                         messages.INFO,
                         'Extension moved to redlist.')
    return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))


@login_required
@admin_required
def redlist_to_greenlist(request, object_id):
    """
    Move the domain in redlist to greenlist.
    """
    try:
        obj = RedEmailList.objects.get(id=object_id)
    except RedEmailList.DoesNotExist:
        return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))
    GreenEmailList.objects.add(obj.extension)
    obj.delete()
    messages.add_message(request,
                         messages.INFO,
                         'Extension moved to greenlist.')
    return HttpResponseRedirect(reverse_lazy('abadmin_show_email_lists'))


@login_required
@admin_required
def rejected_verification_requests(request):
    """
    Show the list of IdCardBasedRejectionProfile objects list.
    ie the list of reject id card based student verification requests.
    """
    objects = IdCardBasedRejectionProfile.objects.all()
    return render(request,
                  'abadmin/rejected_verification_requests.html',
                  {'objects': objects})

@login_required
@admin_required # Needs to be changed to super_admin_required
def show_admin_users(request):
    """
    Lists the admin users in the abcentsir system.
    """
    admin_users = User.objects.filter(is_staff=True)
    return render(request,
                  'abadmin/show_admin_users.html',
                  {'users': admin_users})


@login_required
@admin_required
def add_admin_user(request):
    """
    Adds an admin user. Probably only a super user can do this.
    """
    if request.method == 'POST':
        # Render the form to create a new user
        form = AddAdminUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.add_message(request,
                                 messages.INFO,
                                 'Successfully created user %s' %
                                 user.username)
            return HttpResponseRedirect(reverse_lazy(
                                        'abadmin_show_admin_users'))
    else:
        form = AddAdminUserForm()
    return render(request,
                  'abadmin/add_admin_user.html',
                  {'form': form})


@login_required
@admin_required
def delete_admin_user(request, user_id):
    """
    Soft deletes the user with given id. Set the is_active flag to False.
    """
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return HttpResponseRedirect(reverse_lazy('abadmin_show_admin_users'))
    user.is_active = False
    user.save()
    return HttpResponseRedirect(reverse_lazy('abadmin_show_admin_users'))


# Views related to offers

@login_required
@admin_required
def add_offer(request):
    """
    Displays a form to add offer.
    """
    if request.method == 'POST':
        form = AddOfferForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('abadmin_home'))
    else:
        form = AddOfferForm()
    return render(request,
                  'abadmin/add_offer.html',
                  {'form': form})
    
