from django.shortcuts import render
from endless_pagination.decorators import page_template
from abcentsir.apps.offers.models import Offer


@page_template("offers/list_offer_page.html")
def list_offers(request, template='offers/list_offer.html',
                extra_context=None):
    """
    Lists all the offers as a infinite scroll page.
    """
    context = {'offers': Offer.objects.all()}
    if extra_context is not None:
        context.update(extra_context)
    return render(request,
                  template,
                  context)


