from django.conf.urls import patterns, url


urlpatterns = patterns('abcentsir.apps.offers.views',
        url(r'^offers/$', 'list_offers', name='list_offers'),
        )
