from django.db import models
from tagging.fields import TagField


class Category(models.Model):
    """
    Works as a generic category of offers, webinars and all.
    """
    name = models.CharField(max_length=300)


class SubCategory(models.Model):
    """
    Used as sub categories of offers, webinars and all.
    """
    name = models.CharField(max_length=300)


class Offer(models.Model):
    """
    Fields needed by offers model.
    """
    timestamp = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category)
    #sub_category = models.ForeignKey(SubCategory)
    image = models.ImageField(upload_to='offer_images/%Y/%m/%d',
                              blank=True)
    title = models.CharField(max_length=200)
    sub_title = models.CharField(max_length=200, blank=True)
    scheduled_date = models.DateTimeField(blank=True, null=True)
    detailed_description = models.CharField(max_length=500, blank=True)
    #direct_to_website = models.BooleanField(default=True)
    # Url to which user will be redirected when he clicks Learn More if
    # direct_to_website is True
    website_url = models.URLField()
    #expiry_date = models.DateTimeField(blank=True)
    #tags = TagField()


