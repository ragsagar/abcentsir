from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template, redirect_to
from django.conf import settings
from django.conf.urls.static import static


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^abcentsir/', include('abcentsir.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^about/$', direct_to_template, {'template': 'about.html'},
        name='about'),
    url(r'^policy/$', direct_to_template, {'template': 'policy.html'},
        name='policy'),
    url(r'^terms/$', direct_to_template, {'template': 'terms.html'},
        name='terms'),
    url(r'^ajax/colleges/$', 'abcentsir.apps.auth.views.get_colleges',
        name='ajax_colleges'),
    url(r'^abadmin/', include('abcentsir.apps.abadmin.urls')),
    url(r'', include('social_auth.urls')),
    url(r'', include('abcentsir.apps.auth.urls')),
    url(r'', include('abcentsir.apps.offers.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
